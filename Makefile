##
## Makefile for malloc in /home/jordan/ETNA/DEVC/malloc
##
## Made by VEROVE Jordan
## Login   <verove_j@etna-alternance.net>
##
## Started on  Thu Feb  2 09:29:53 2017 VEROVE Jordan
## Last update Sat May  2 17:18:57 2020 VEROVE Jordan
##

OS:=		$(shell uname)

SRCS_DIR =	srcs/

SRCS =		$(SRCS_DIR)alloc.c \
		$(SRCS_DIR)calloc.c \
		$(SRCS_DIR)find.c \
		$(SRCS_DIR)free.c \
		$(SRCS_DIR)insert.c \
		$(SRCS_DIR)malloc.c \
		$(SRCS_DIR)realloc.c

OBJS =		$(SRCS:.c=.o)

NAME =		libmy_malloc_$(OS).so

LIB_MAL =	./libmy_malloc.so

RM =		rm -f

CC =		gcc -o $(NAME)

INCLUDES =	-Iincludes -I.

CFLAGS =	-fpic -W -Wall -Werror -Wextra $(INCLUDES)

$(NAME):	$(OBJS)
		$(CC) $(INCLUDES) -shared -o $(NAME) $(OBJS)
		ln -sf $(NAME) $(LIB_MAL)

all:		$(NAME)

clean:
		rm -f $(OBJS)

fclean:		clean
		rm -f $(NAME)
		rm -f $(LIB_MAL)

re:		fclean all

.PHONY:		all clean fclean re
