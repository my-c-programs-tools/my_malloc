/*
** find.c for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Mon Jan 23 18:20:27 2017 VEROVE Jordan
** Last update Fri Oct 18 09:20:32 2019 VEROVE Jordan
*/

#include "malloc.h"
#include "malloc.conf.h"

t_block		*find_size_in_hash(size_t size)
{
  int		index;
  t_block	*block;

  index = FREE_IND(size);
  block = g_free_tab[index];
  while (block && block->size < size)
    block = block->next;
  if (block)
    {
      if (!block->prev)
	g_free_tab[index] = block->next;
      else
	block->prev->next = block->next;
      if (block->next)
	block->next->prev = block->prev;
      block->prev = NULL;
      block->next = NULL;
    }
  return (block);
}

t_block		*find_free_in_hash(t_block *target)
{
  int		index;
  t_block	*tmp;

  index = FREE_IND(target->size);
  tmp = g_free_tab[index];
  while (tmp && tmp != target)
    tmp = tmp->next;
  if (tmp)
    {
      if (!tmp->prev)
	g_free_tab[index] = tmp->next;
      else
	tmp->prev->next = tmp->next;
      if (tmp->next)
	tmp->next->prev = tmp->prev;
      tmp->prev = NULL;
      tmp->next = NULL;
    }
  return (tmp);
}

t_block		*find_alloc_in_hash(void *addr)
{
  int		index;
  t_block	*block;

  index = ALLOC_IND(addr);
  block = g_alloc_tab[index];
  while (block && block->start != addr)
    block = block->next;
  if (block)
    {
      if (!block->prev)
	g_alloc_tab[index] = block->next;
      else
	block->prev->next = block->next;
      if (block->next)
	block->next->prev = block->prev;
      block->prev = NULL;
      block->next = NULL;
    }
  return (block);
}

t_block		*find_free_block(size_t size)
{
  t_block	*block;

  if ((block = find_size_in_hash(size)) != NULL ||
      (block = alloc_new_page(size)) != NULL)
    create_from_free(block, size);
  return (block);
}
