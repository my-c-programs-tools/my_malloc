/*
** calloc.c for malloc in /home/jordan/ETNA/DEVC/malloc
** 
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
** 
** Started on  Thu Feb  9 14:13:36 2017 VEROVE Jordan
** Last update Fri Oct 18 09:41:47 2019 VEROVE Jordan
*/

#include <string.h>
#include "malloc.h"

void	*calloc(size_t nmemb, size_t size)
{
  void	*ptr;

  if ((ptr = malloc(nmemb * size)) == NULL)
    return (NULL);
  memset(ptr, 0, nmemb * size);
  return (ptr);
}
