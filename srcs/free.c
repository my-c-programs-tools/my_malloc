/*
** free.c for malloc in /home/jordan/rendu/Malloc/my-malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Wed Feb  1 08:43:36 2017 VEROVE Jordan
** Last update Fri Oct 18 09:41:10 2019 VEROVE Jordan
*/

#include "malloc.conf.h"
#include "malloc.h"

t_block		*merge_next_block(t_block *block)
{
  t_block	*target;
  t_block	*next_block;

  next_block = block->start + block->size;
  if ((intptr_t)((char*)next_block + sizeof(t_block)) < (intptr_t)g_heap_end &&
      (target = find_free_in_hash(next_block)) != NULL)
    block->size += target->size + sizeof(t_block);
  return (block);
}

void		free(void *ptr)
{
  t_block	*target;

  if (ptr == NULL ||
      (intptr_t)ptr <= (intptr_t)(g_heap_start - sizeof(t_block)) ||
      (intptr_t)ptr >= (intptr_t)(g_heap_end + ALIGN(1)))
    if ((target = find_alloc_in_hash(ptr)) != NULL)
      {
	target = merge_next_block(target);
	insert_block_in_free(target);
      }
}
