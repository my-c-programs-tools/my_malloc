/*
** realloc.c for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Thu Feb  9 14:11:49 2017 VEROVE Jordan
** Last update Fri Oct 18 09:22:29 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include <string.h>
#include "malloc.conf.h"
#include "malloc.h"

void		*realloc(void *ptr, size_t size)
{
  t_block	*target;
  void		*new;

  if (ptr == NULL)
    return (malloc(size));
  if (size == 0 || (target = find_alloc_in_hash(ptr)) == NULL)
    return (NULL);
  if (target->size >= size)
    return (ptr);
  target = merge_next_block(target);
  if (target->size < size && (new = malloc(size)) != NULL)
    {
      memcpy(new, target->start, size);
      free(target->start);
      target = new - sizeof(t_block);
    }
  else
    {
      create_from_free(target, size);
      insert_block_in_alloc(target);
    }
  return (target->start);
}
