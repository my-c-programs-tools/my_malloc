/*
** alloc.c for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Tue Jan 31 17:59:30 2017 VEROVE Jordan
** Last update Fri Oct 18 09:19:14 2019 VEROVE Jordan
*/

#include <unistd.h>
#include "malloc.h"
#include "malloc.conf.h"

static void	split_block(t_block *block, size_t size)
{
  t_block	*new;

  new = (t_block*)((char*)block->start + size);
  new->size = block->size - size - sizeof(t_block);
  new->start = (char*)new + sizeof(t_block);
  new->next = NULL;
  new->prev = NULL;
  block->size = size;
  insert_block_in_free(new);
}

t_block		*alloc_new_page(size_t size)
{
  int	        needed;
  int	        nb_page;
  int	        new_size;
  t_block	*target;

  needed = size + sizeof(t_block);
  nb_page = needed / PAGESIZE;
  nb_page += ((needed % PAGESIZE) == 0) ? (0) : (1);
  new_size = nb_page * PAGESIZE;
  if ((intptr_t)(target = sbrk(new_size)) == -1)
    return (NULL);
  target->size = new_size - sizeof(t_block);
  target->start = (char*)target + sizeof(t_block);
  target->prev = NULL;
  target->next = NULL;
  g_heap_end = (void*)target + new_size;
  return (target);
}

void		create_from_free(t_block *block, size_t size)
{
  long int      rest;
  long int	minimum;

  rest = block->size - (size + sizeof(t_block));
  minimum = ALIGN(1) + sizeof(t_block);
  if (rest >= minimum)
    split_block(block, size);
}
