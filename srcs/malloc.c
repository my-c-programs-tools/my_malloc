/*
** malloc.c for malloc in /home/jordan/rendu/Malloc/verove_j
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Fri Jan 13 14:27:19 2017 VEROVE Jordan
** Last update Fri Oct 18 09:36:42 2019 VEROVE Jordan
*/

#include <stdlib.h>
#include <unistd.h>

#include "malloc.conf.h"
#include "malloc.h"

t_block	*g_alloc_tab[HASH_SIZE];
t_block	*g_free_tab[HASH_SIZE];
void	*g_heap_start;
void	*g_heap_end;

static void	init_malloc()
{
  int		i;

  g_heap_start = sbrk(0);
  g_heap_end = g_heap_start;
  i = 0;
  while (i < HASH_SIZE)
    {
      g_alloc_tab[i] = NULL;
      g_free_tab[i] = NULL;
      ++i;
    }
  return ;
}

void		*malloc(size_t size)
{
  t_block	*block;

  /* lock mutex */
  if (size == 0)
    return (NULL);
  size = ALIGN(size);
  if (PAGESIZE == 0)
    init_malloc();
  if ((block = find_free_block(size)) != NULL)
    insert_block_in_alloc(block);
  /* unlock mutex */
  else
    return (NULL);
  return (block->start);
}
