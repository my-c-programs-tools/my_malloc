/*
** insert.c for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Wed Jan 25 17:43:23 2017 VEROVE Jordan
** Last update Fri Oct 18 09:21:14 2019 VEROVE Jordan
*/

#include "malloc.conf.h"
#include "malloc.h"

static t_block	*add_to_tail(t_block *list, t_block *block)
{
  t_block	*tmp;

  tmp = NULL;
  if (list)
    {
      tmp = list;
      while (tmp && tmp->next)
	tmp = tmp->next;
      tmp->next = block;
    }
  else
    list = block;
  block->prev = tmp;
  return (list);
}

void	insert_block_in_alloc(t_block *block)
{
  int	index;

  index = ALLOC_IND(block->start);
  g_alloc_tab[index] = add_to_tail(g_alloc_tab[index], block);
  return ;
}

void	insert_block_in_free(t_block *block)
{
  int	index;

  index = FREE_IND(block->size);
  g_free_tab[index] = add_to_tail(g_free_tab[index], block);
  return ;
}
