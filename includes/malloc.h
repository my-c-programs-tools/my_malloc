/*
** malloc.h for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Wed Jan 18 17:51:58 2017 VEROVE Jordan
** Last update Fri Oct 18 09:36:25 2019 VEROVE Jordan
*/

#ifndef			_MALLOC_H_
# define		_MALLOC_H_

# include		<stdlib.h>

typedef struct		s_block
{
  size_t		size;
  void			*start;
  struct s_block	*next;
  struct s_block	*prev;
}			t_block;

extern t_block		*g_alloc_tab[];
extern t_block		*g_free_tab[];
extern int		g_pagesize;
extern void		*g_heap_start;
extern void		*g_heap_end;

/*
** alloc.c
*/
t_block			*alloc_new_page(size_t);
void			create_from_free(t_block*, size_t);

/*
** calloc.c
*/
void			*calloc(size_t, size_t);

/*
** find.c
*/
t_block			*find_free_block(size_t);
t_block			*find_alloc_in_hash(void*);
t_block			*find_size_in_hash(size_t);
t_block			*find_free_in_hash(t_block*);

/*
** free.c
*/
t_block			*merge_next_block(t_block*);
void			free(void*);

/*
** insert.c
*/
void			insert_block_in_alloc(t_block*);
void			insert_block_in_free(t_block*);

/*
** malloc.c
*/
void			*malloc(size_t);

/*
** realloc.c
*/
void			*realloc(void*, size_t);

#endif			/* !_MALLOC_H_ */
