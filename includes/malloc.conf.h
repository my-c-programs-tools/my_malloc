/*
** malloc.conf.h for malloc in /home/jordan/ETNA/DEVC/malloc
**
** Made by VEROVE Jordan
** Login   <verove_j@etna-alternance.net>
**
** Started on  Wed Jan 18 18:11:38 2017 VEROVE Jordan
** Last update Fri Oct 18 09:32:52 2019 VEROVE Jordan
*/

#ifndef		_MALLOC_CONF_H_
# define	_MALLOC_CONF_H_

# include	<stdint.h>

# define	PAGESIZE	(4096)
# define	HASH_SIZE	(PAGESIZE / 16)
# define	MIN(a, b)	(a < b) ? (a) : (b)
# define	ALIGN(nb)	(nb % 16 == 0) ? (nb) : (nb + 16 - (nb % 16))
# define	INT(addr)	((intptr_t)addr)
# define	ALLOC_IND(addr)	(int)(((INT(addr) - 16) / 16) % HASH_SIZE)
# define        FREE_IND(size)	(int)MIN(size / PAGESIZE, 255)

#endif		/* !_MALLOC_CONF_H_ */
